﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaLogica
{
    public class AgregarContacto
    {
        private long telefono;
        private string nombre;
        private string usuario;
        private string clave;
        private PictureBox imagen;

        public SqlDataAdapter Clt;
        public DataTable TProducto;
        public SqlCommand comandoProducto;
        CapaDatos.Conexion oConex = new CapaDatos.Conexion();

        //public SqlDataReader lector;//Leer la base de datos
        public SqlCommand con;//Escribir en la base de datos
        #region get
        public long Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public string Clave
        {
            get
            {
                return clave;
            }

            set
            {
                clave = value;
            }
        }

        public PictureBox Imagen
        {
            get
            {
                return imagen;
            }

            set
            {
                imagen = value;
            }
        }
        #endregion

        public string Agregar( long telefono , string nombre , string usuario ,string clave , PictureBox imagen)
        {
            string mensaje = "Se inserto la imagen";
            try
            {
                con = new SqlCommand("Insert into Contactos values(" + telefono + ",'" + nombre + "','" + usuario + "','" + clave + "','" + imagen + "')", oConex.conecte());
                
                System.IO.MemoryStream ms = new System.IO.MemoryStream();

                imagen.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                con.Parameters["@Imagen"].Value = ms.GetBuffer();
                con.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                mensaje = "No se inserto la imagen: " + ex.ToString();
            }
            return mensaje;
        }
    }
}
